<?php

// if (!session_id()) session_start();

require_once 'init.php';

//CONFIGURATION for SmartAdmin UI

//ribbon breadcrumbs config
//array("Display Name" => "URL");
$breadcrumbs = array(
    "Home" => APP_URL
);

/*navigation array config

ex:
"dashboard" => array(
    "title" => "Display Title",
    "url" => "http://yoururl.com",
    "url_target" => "_self",
    "icon" => "fa-home",
    "label_htm" => "<span>Add your custom label/badge html here</span>",
    "sub" => array() //contains array of sub items with the same format as the parent
)

*/
$page_nav = array(
    "painel" => array(
        "title" => "Painel",
        "icon" => "fa-dashboard",
        "url" => "ajax/dashboard"
    ),
    "agenda" => array(
        "title" => "Agenda",
        "icon" => "fa-calendar",
        "url" => "ajax/agenda"
    ),
    "clients" => array(
        "title" => "Clientes",
        "icon" => "fa-group",
        "url" => "ajax/customers",
        "sub" => array(
            "painel" => array(
                "title" => "Novo Cliente",
                "icon" => "fa-user",
                "url" => "ajax/customerForm",
                "action" => "edit",
                "show" => true
            ),
        )
    ),
    "projects" => array(
        "title" => "Projetos",
        "icon" => "fa-camera-retro",
        "url" => "ajax/projects"
    ),
    "contracts" => array(
        "title" => "Contratos",
        "icon" => "fa-edit",
        "url" => "ajax/contracts"
    )
);

//configuration variables
$page_title = "CRM";
$page_css = array();
$no_main_header = false; //set true for lock.php and login.php
$page_body_prop = array(); //optional properties for <body>
$page_html_prop = array(); //optional properties for <html>
?>