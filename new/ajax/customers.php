<?php require_once 'inc/init.php'; ?>
<!-- row -->
<div class="row">

	<!-- col -->
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<!-- PAGE HEADER -->
			<i class="fa-fw fa fa-home"></i>
			CRM
			<span>>
				Cadastro Clientes
			</span>
		</h1>
	</div>
	<!-- end col -->
</div>
<!-- end row -->

<!-- row -->
<div class="row">
	<!-- a blank row to get started -->
	<div class="col-sm-12">
		<!-- Widget ID (each widget will need unique ID)-->
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"
			-->
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Lista de Contatos</h2>

			</header>

			<!-- widget div-->
			<div>

				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->

				</div>
				<!-- end widget edit box -->

				<!-- widget content -->
				<div class="widget-body no-padding">

					<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
						<thead>
							<tr>
								<th>#</th>
								<th data-class="expand">Nome</th>
								<th>E-mail</th>
								<th data-hide="phone">Celular</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody id="body_dataset">
							<tr>
								<td>989</td>
								<td>José Silva</td>
								<td>jose@silva.com</td>
								<td>9 9999-9999</td>
								<td>
									<a class="btn btn-xs btn-success" data-original-title="Editar" onclick="" href="#ajax/customerForm.php"><i class="fa fa-edit"></i> &nbsp;Editar</a>
									<button class="btn btn-xs btn-info" data-original-title="Visualizar" onclick="openModal('users','edit',8)"><i class="fa fa-eye"></i> &nbsp;Visualizar</button>
									<button class="btn btn-xs btn-danger" data-original-title="Cancel" onclick="openModal('users','delete',4)"><i class="fa fa-times"></i> &nbsp;Excluir</button>
									<button class="btn btn-xs btn-default" data-original-title="Erro" onclick="openModal()"><i class="fa fa-times"></i> &nbsp; Simular erro</button>
								</td>				
						</tbody>
					</table>
				</div>
				<!-- end widget content -->

			</div>
			<!-- end widget div -->

		</div>
		<!-- end widget -->
		<!-- confirm-dialog -->
		<div id="dialog_delete" title="Confirmar ação">
			<p>
				<span id="ajax_data_delete"></span>
			</p>
			<p>
				<b>Atenção: </b> Esta operação não pode ser desfeita .
			</p>
		</div>
		<!-- see-dialog -->
		<div id="dialog_see" title="Ver item">
				<p>
					<span id="ajax_data_edit"></span></p>
		</div>
		<!-- see-dialog -->
		<div id="dialog_error" title="Erro">
					<p>
						<b>Desculpe-nos.&nbsp;&nbsp;&nbsp;</b>Ocorreu um erro ao executar sua solicitação.<span id="ajax_data_error"></span></p>
		</div>
	</div>
	</div>
		<!-- end row -->

		<script type="text/javascript">
			$("#dialog_delete").hide();
			$("#dialog_error").hide();
			$("#dialog_see").hide();

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

	 pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 * TO LOAD A SCRIPT:
	 * var pagefunction = function (){
	 *  loadScript(".../plugin.js", run_after_loaded);
	 * }
	 *
	 * OR you can load chain scripts by doing
	 *
	 * loadScript(".../plugin.js", function(){
	 * 	 loadScript("../plugin.js", function(){
	 * 	   ...
	 *   })
	 * });
	 */

	// pagefunction
	var pagefunction = function() {
		$.ajax({
				url: 'https://jsonplaceholder.typicode.com/users',
				success: function(data) {
					var content = "";
					var actionDomainEdit = "\'users\',\'edit\',";
					var actionDomainDelete = "\'users\',\'delete\',";
					$.each( data, function( i, val ) {
								content += '<tr><td>' + data[i].id + '</td><td>' + data[i].name + '</td><td>' + data[i].email + '</td><td>' + data[i].phone + '</td>';
								content += '<td><a class="btn btn-xs btn-success" data-original-title="Editar" onclick="" href="#ajax/customerForm.php"><i class="fa fa-edit"></i>   Editar  </a>'
								content += '<button class="btn btn-xs btn-info" data-original-title="Visualizar" onclick="openModal('+actionDomainEdit + data[i].id +')"><i class="fa fa-eye"></i>  Visualizar   </button>'
								content += '<button class="btn btn-xs btn-danger" data-original-title="Cancel" onclick="openModal('+actionDomainDelete + data[i].id +')"><i class="fa fa-times"></i>  Excluir&nbsp;</button>'
								content += '</td></tr>';
					});
					$("#body_dataset").append(content);
					// clears the variable if left blank
						var responsiveHelper_datatable_fixed_column = undefined;
						var breakpointDefinition = {
							tablet : 1024,
							phone : 480
						};

						/* COLUMN FILTER  */
						var otable = $('#datatable_fixed_column').DataTable({
								//"bFilter": false,
								//"bInfo": false,
								//"bLengthChange": false
								//"bAutoWidth": false,
								//"bPaginate": false,
								//"bStateSave": true // saves sort state using localStorage
								"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"autoWidth" : true,
								"preDrawCallback" : function() {
								// Initialize the responsive datatables helper once.
								if (!responsiveHelper_datatable_fixed_column) {
									responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
								}
							},
							"rowCallback" : function(nRow) {
								responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
							},
							"drawCallback" : function(oSettings) {
								responsiveHelper_datatable_fixed_column.respond();
							},
							"paging": true,
							"searching": true
						});

						// custom toolbar
							// $("div.toolbar").html('<div class="text-right"><img src="img/loago.png" alt="CRM" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

						// Apply the filter
						$("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
							otable
							.column( $(this).parent().index()+':visible' )
							.search( this.value )
							.draw();

						} );
						/* END COLUMN FILTER */
					}
		});
	};
	// end pagefunction
	
	// loads
	loadScript("js/plugin/datatables/jquery.dataTables.min.js", function(){
			// loadScript("js/plugin/datatables/dataTables.colVis.min.js", function(){
				// loadScript("js/plugin/datatables/dataTables.tableTools.min.js", function(){
					loadScript("js/plugin/datatables/dataTables.bootstrap.min.js", function(){
						loadScript("js/plugin/datatable-responsive/datatables.responsive.min.js", function() {
							loadScript("https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js", function() {
								loadScript("https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js", function() {
									// loadScript("https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js", function() {
										// loadScript("https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js", function() {
											// loadScript("https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js", function() {
												// loadScript("https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js", function() {
													loadScript("https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js", pagefunction)
												// })
											})
							})
									// })
								})
							// });
						})
				});
				//});
			//});
	//});

	// Modals
	$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
		_title : function(title) {
			if (!this.options.title) {
				title.html("&#160;");
			} else {
				title.html(this.options.title);
			}
		}
	}));

	$('#dialog_error').dialog({
			autoOpen : false,
			width : 500,
			resizable : false,
			modal : true,
			title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Ops ... </h4></div>",
			buttons : [
			{
				html : "<i class='fa fa-times'></i>&nbsp; Fechar",
				"class" : "btn btn-default",
				click : function() {
					$(this).dialog("close");
				}
			}]
	});

	$('#dialog_see').dialog({
		autoOpen : false,
		width : 600,
		resizable : false,
		modal : true,
		title : "<div class='widget-header'><h4> Visualizar Cadastro </h4></div>",
		buttons : [
		{
			html : "<i class='fa fa-times'></i>&nbsp; Fechar",
			"class" : "btn btn-default",
			click : function() {
				$(this).dialog("close");
			}
		}]
	});
	// End Modals
	$('#dialog_delete').dialog({
		autoOpen : false,
		width : 500,
		resizable : false,
		modal : true,
		title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Deseja remover este registro ?</h4></div>",
		buttons : [{
			html : "<i class='fa fa-trash-o'></i>&nbsp; Excluir",
			"class" : "btn btn-danger",
			click : function() {
				$(this).dialog("close");
			}
		}, 
		{
			html : "<i class='fa fa-times'></i>&nbsp; Cancelar",
			"class" : "btn btn-default",
			click : function() {
				$(this).dialog("close");
			}
		}]
	});

	function openModal(domain,action,id) {
		if(typeof domain == typeof undefined | typeof action == typeof undefined | typeof id == typeof undefined) {
			$('#dialog_error').dialog('open');
		}

		if(action == 'edit'){
			var userFind = "https://jsonplaceholder.typicode.com/users/" + id +"";
			$.ajax({
				url: userFind,
				success: function(data) {
					var content = "";
					content += "<p><b>Nome : </b>" + data.name + "</p>";
					content += "<p><b>E-mail : </b>" + data.email + "</p>";
					content += "<p><b>Telefone : </b>" + data.phone + "</p>";
					$("#ajax_data_edit").html(content);
					$('#dialog_see').dialog('open');
				}
			});
		}
		if(action == 'delete') {
			var userFind = "https://jsonplaceholder.typicode.com/users/" + id +"";
			$.ajax({
				url: userFind,
				success: function(data) {
					$("#ajax_data_delete").html("Excuir ID " + id +" ?");
					$('#dialog_delete').dialog('open');
				}
			});
		}
	}

</script>