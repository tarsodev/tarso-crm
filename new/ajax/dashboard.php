<?php require_once 'inc/init.php'; ?>
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Home <span>> Painel</span></h1>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
		<ul id="sparks" class="">
			<li class="sparks-info">
				<h5> Lucros <span class="txt-color-blue">R$3,200</span></h5>
				<div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
					1300, 1877, 2500, 2577, 2000
				</div>
			</li>
			<li class="sparks-info">
				<h5> Novos Projetos <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"></i>&nbsp;15%</span></h5>
				<div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
					110,150,300,130,400,240,
				</div>
			</li>
			<li class="sparks-info">
				<h5> Vendas <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;3</span></h5>
				<div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
					110,150,300,130,400,240,220,310,220
				</div>
			</li>
		</ul>
	</div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">



	<!-- row -->
	

	<!-- new widget -->
	<div class="jarviswidget jarviswidget-color-blue" id="wid-id-4" data-widget-editbutton="false" data-widget-colorbutton="false">

				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

			-->

			<header>
				<span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>
				<h2> Pendências </h2>
					<!-- <div class="widget-toolbar">
					add: non-hidden - to disable auto hide

				</div>-->
			</header>

			<!-- widget div-->
			<div>
				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<div>
						<label>Título:</label>
						<input type="text" />
					</div>
				</div>
				<!-- end widget edit box -->

				<div class="widget-body no-padding smart-form">
					<!-- content goes here -->
					<h5 class="todo-group-title"><i class="fa fa-warning"></i> Urgentes (<small class="num-of-tasks">1</small>)</h5>
					<ul id="sortable1" class="todo">
						<li>
							<span class="handle"> <label class="checkbox">
								<input type="checkbox" name="checkbox-inline">
								<i></i> </label> </span>
								<p>
									<strong>Item #1532</strong> - Ensaio Mariana [<a href="javascript:void(0);" class="font-xs">Detalhes</a>] <span class="text-muted">Total fotos : 198. </span>
									<span class="date">01/06/2018</span>
								</p>
							</li>
						</ul>
						<h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Importantes (<small class="num-of-tasks">3</small>)</h5>
						<ul id="sortable2" class="todo">
							<li>
								<span class="handle"> <label class="checkbox">
									<input type="checkbox" name="checkbox-inline">
									<i></i> </label> </span>
									<p>
										<strong>Item #1563</strong> - Erro no tratamento <small>(correção)</small> [<a href="javascript:void(0);" class="font-xs">Detalhes</a>] <span class="date">01/06/2017</span>
									</p>
								</li>
								<li>
									<span class="handle"> <label class="checkbox">
										<input type="checkbox" name="checkbox-inline">
										<i></i> </label> </span>
										<p>
											<strong>Item #1563</strong> - Tratar fotos iniciais <small>(correção)</small> [<a href="javascript:void(0);" class="font-xs">Detalhes</a>] <span class="date">01/06/2017</span>
										</p>
									</li>
									<li>
										<span class="handle"> <label class="checkbox">
											<input type="checkbox" name="checkbox-inline">
											<i></i> </label> </span>
											<p>
												<strong>Item #1565</strong> - Gerar rascunho ensaio <small>(correção)</small> [<a href="javascript:void(0);" class="font-xs">Detalhes</a>] <span class="date">Finalizar em 01/06/2017</span>
											</p>
										</li>
									</ul>

									<h5 class="todo-group-title"><i class="fa fa-check"></i> Finalizadas (<small class="num-of-tasks">1</small>)</h5>
									<ul id="sortable3" class="todo">
										<li class="complete">
											<span class="handle"> <label class="checkbox state-disabled" style="display:none">
												<input type="checkbox" name="checkbox-inline" checked="checked" disabled="disabled">
												<i></i> </label> </span>
												<p>
													<strong>Item #1566</strong> - Fechamento álbum [<a href="javascript:void(0);" class="font-xs">Detalhes</a>] <span class="text-muted">Finalizado com sucesso. </span>
													<span class="date">01/06/2018</span>
												</p>
											</li>
										</ul>

										<!-- end content -->
									</div>

								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->

						</article>

					</div>

					<!-- end row -->

				</section -->
				<!-- end widget grid -->

				<script type="text/javascript">
	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

	 var flot_updating_chart, flot_statsChart, flot_multigraph, calendar;

	 pageSetUp();
	 
	/*
	 * PAGE RELATED SCRIPTS
	 */

	// pagefunction
	
	var pagefunction = function() {
		
		$(".js-status-update a").click(function () {
			var selText = $(this).text();
			var $this = $(this);
			$this.parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
			$this.parents('.dropdown-menu').find('li').removeClass('active');
			$this.parent().addClass('active');
		});
		
		/*
		 * TODO: add a way to add more todo's to list
		 */
		 
		// initialize sortable
		
		$("#sortable1, #sortable2").sortable({
			handle: '.handle',
			connectWith: ".todo",
			update: countTasks
		}).disableSelection();
		
		
		// check and uncheck
		$('.todo .checkbox > input[type="checkbox"]').click(function () {
			var $this = $(this).parent().parent().parent();
			
			if ($(this).prop('checked')) {
				$this.addClass("complete");
				
		        // remove this if you want to undo a check list once checked
		        //$(this).attr("disabled", true);
		        $(this).parent().hide();
		        
		        // once clicked - add class, copy to memory then remove and add to sortable3
		        $this.slideUp(500, function () {
		        	$this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
		        	$this.remove();
		        	countTasks();
		        });
		    } else {
		        // insert undo code here...
		    }
		    
		})
		// count tasks
		function countTasks() {
			
			$('.todo-group-title').each(function () {
				var $this = $(this);
				$this.find(".num-of-tasks").text($this.next().find("li").size());
			});
			
		}
		
		/*
		 * RUN PAGE GRAPHS
		 */

		// load all flot plugins
		loadScript("js/plugin/flot/jquery.flot.cust.min.js", function(){
			loadScript("js/plugin/flot/jquery.flot.resize.min.js", function(){
				loadScript("js/plugin/flot/jquery.flot.time.min.js", function(){
					loadScript("js/plugin/flot/jquery.flot.tooltip.min.js", generatePageGraphs);
				});
			});
		});

		
		function generatePageGraphs() {
			
			/* TAB 1: UPDATING CHART */
		    // For the demo we use generated data, but normally it would be coming from the server
		    
		    var data = [],
		    totalPoints = 200,
		    $UpdatingChartColors = $("#updating-chart").css('color');
		    
		    function getRandomData() {
		    	if (data.length > 0)
		    		data = data.slice(1);
		    	
		        // do a random walk
		        while (data.length < totalPoints) {
		        	var prev = data.length > 0 ? data[data.length - 1] : 50;
		        	var y = prev + Math.random() * 10 - 5;
		        	if (y < 0)
		        		y = 0;
		        	if (y > 100)
		        		y = 100;
		        	data.push(y);
		        }
		        
		        // zip the generated y values with the x values
		        var res = [];
		        for (var i = 0; i < data.length; ++i)
		        	res.push([i, data[i]])
		        return res;
		    }
		    
		    // setup control widget
		    var updateInterval = 1500;
		    $("#updating-chart").val(updateInterval).change(function () {
		    	
		    	var v = $(this).val();
		    	if (v && !isNaN(+v)) {
		    		updateInterval = +v;
		    		$(this).val("" + updateInterval);
		    	}
		    	
		    });
		    
		    // setup plot
		    var options = {
		    	yaxis: {
		    		min: 0,
		    		max: 100
		    	},
		    	xaxis: {
		    		min: 0,
		    		max: 100
		    	},
		    	colors: [$UpdatingChartColors],
		    	series: {
		    		lines: {
		    			lineWidth: 1,
		    			fill: true,
		    			fillColor: {
		    				colors: [{
		    					opacity: 0.4
		    				}, {
		    					opacity: 0
		    				}]
		    			},
		    			steps: false
		    			
		    		}
		    	}
		    };
		    
		    flot_updating_chart = $.plot($("#updating-chart"), [getRandomData()], options);
		    
		    /* live switch */
		    $('input[type="checkbox"]#start_interval').click(function () {
		    	if ($(this).prop('checked')) {
		    		$on = true;
		    		updateInterval = 1500;
		    		update();
		    	} else {
		    		clearInterval(updateInterval);
		    		$on = false;
		    	}
		    });
		    
		    function update() {

		    	try {
		    		if ($on == true) {
		    			flot_updating_chart.setData([getRandomData()]);
		    			flot_updating_chart.draw();
		    			setTimeout(update, updateInterval);
		    			
		    		} else {
		    			clearInterval(updateInterval)
		    		}
		    	}
		    	catch(err) {
		    		clearInterval(updateInterval);
		    	}
		    	
		    }
		    
		    var $on = false;
		    
		    /*end updating chart*/
		    
		    /* TAB 2: Social Network  */
		    
		    $(function () {
		        // jQuery Flot Chart
		        var twitter = [
		        [1, 27],
		        [2, 34],
		        [3, 51],
		        [4, 48],
		        [5, 55],
		        [6, 65],
		        [7, 61],
		        [8, 70],
		        [9, 65],
		        [10, 75],
		        [11, 57],
		        [12, 59],
		        [13, 62]
		        ],
		        facebook = [
		        [1, 25],
		        [2, 31],
		        [3, 45],
		        [4, 37],
		        [5, 38],
		        [6, 40],
		        [7, 47],
		        [8, 55],
		        [9, 43],
		        [10, 50],
		        [11, 47],
		        [12, 39],
		        [13, 47]
		        ],
		        data = [{
		        	label: "Twitter",
		        	data: twitter,
		        	lines: {
		        		show: true,
		        		lineWidth: 1,
		        		fill: true,
		        		fillColor: {
		        			colors: [{
		        				opacity: 0.1
		        			}, {
		        				opacity: 0.13
		        			}]
		        		}
		        	},
		        	points: {
		        		show: true
		        	}
		        }, {
		        	label: "Facebook",
		        	data: facebook,
		        	lines: {
		        		show: true,
		        		lineWidth: 1,
		        		fill: true,
		        		fillColor: {
		        			colors: [{
		        				opacity: 0.1
		        			}, {
		        				opacity: 0.13
		        			}]
		        		}
		        	},
		        	points: {
		        		show: true
		        	}
		        }];
		        
		        var options = {
		        	grid: {
		        		hoverable: true
		        	},
		        	colors: ["#568A89", "#3276B1"],
		        	tooltip: true,
		        	tooltipOpts: {
		                //content : "Value <b>$x</b> Value <span>$y</span>",
		                defaultTheme: false
		            },
		            xaxis: {
		            	ticks: [
		            	[1, "JAN"],
		            	[2, "FEB"],
		            	[3, "MAR"],
		            	[4, "APR"],
		            	[5, "MAY"],
		            	[6, "JUN"],
		            	[7, "JUL"],
		            	[8, "AUG"],
		            	[9, "SEP"],
		            	[10, "OCT"],
		            	[11, "NOV"],
		            	[12, "DEC"],
		            	[13, "JAN+1"]
		            	]
		            },
		            yaxes: {
		            	
		            }
		        };
		        
		        flot_statsChart = $.plot($("#statsChart"), data, options);
		    });
		    
		    // END TAB 2
		    
		    // TAB THREE GRAPH //
		    /* TAB 3: Revenew  */
		    
		    $(function () {
		    	
		    	var trgt = [
		    	[1354586000000, 153],
		    	[1364587000000, 658],
		    	[1374588000000, 198],
		    	[1384589000000, 663],
		    	[1394590000000, 801],
		    	[1404591000000, 1080],
		    	[1414592000000, 353],
		    	[1424593000000, 749],
		    	[1434594000000, 523],
		    	[1444595000000, 258],
		    	[1454596000000, 688],
		    	[1464597000000, 364]
		    	],
		    	prft = [
		    	[1354586000000, 53],
		    	[1364587000000, 65],
		    	[1374588000000, 98],
		    	[1384589000000, 83],
		    	[1394590000000, 980],
		    	[1404591000000, 808],
		    	[1414592000000, 720],
		    	[1424593000000, 674],
		    	[1434594000000, 23],
		    	[1444595000000, 79],
		    	[1454596000000, 88],
		    	[1464597000000, 36]
		    	],
		    	sgnups = [
		    	[1354586000000, 647],
		    	[1364587000000, 435],
		    	[1374588000000, 784],
		    	[1384589000000, 346],
		    	[1394590000000, 487],
		    	[1404591000000, 463],
		    	[1414592000000, 479],
		    	[1424593000000, 236],
		    	[1434594000000, 843],
		    	[1444595000000, 657],
		    	[1454596000000, 241],
		    	[1464597000000, 341]
		    	],
		    	toggles = $("#rev-toggles"),
		    	target = $("#flotcontainer");
		    	
		    	var data = [{
		    		label: "Target Profit",
		    		data: trgt,
		    		bars: {
		    			show: true,
		    			align: "center",
		    			barWidth: 30 * 30 * 60 * 1000 * 80
		    		}
		    	}, {
		    		label: "Actual Profit",
		    		data: prft,
		    		color: '#3276B1',
		    		lines: {
		    			show: true,
		    			lineWidth: 3
		    		},
		    		points: {
		    			show: true
		    		}
		    	}, {
		    		label: "Actual Signups",
		    		data: sgnups,
		    		color: '#71843F',
		    		lines: {
		    			show: true,
		    			lineWidth: 1
		    		},
		    		points: {
		    			show: true
		    		}
		    	}]
		    	
		    	var options = {
		    		grid: {
		    			hoverable: true
		    		},
		    		tooltip: true,
		    		tooltipOpts: {
		                //content: '%x - %y',
		                //dateFormat: '%b %y',
		                defaultTheme: false
		            },
		            xaxis: {
		            	mode: "time"
		            },
		            yaxes: {
		            	tickFormatter: function (val, axis) {
		            		return "$" + val;
		            	},
		            	max: 1200
		            }
		            
		        };
		        
		        flot_multigraph = null;
		        
		        function plotNow() {
		        	var d = [];
		        	toggles.find(':checkbox').each(function () {
		        		if ($(this).is(':checked')) {
		        			d.push(data[$(this).attr("name").substr(4, 1)]);
		        		}
		        	});
		        	if (d.length > 0) {
		        		if (flot_multigraph) {
		        			flot_multigraph.setData(d);
		        			flot_multigraph.draw();
		        		} else {
		        			flot_multigraph = $.plot(target, d, options);
		        		}
		        	}
		        	
		        };
		        
		        toggles.find(':checkbox').on('change', function () {
		        	plotNow();
		        });

		        plotNow()
		        
		    });
		    
		}
		
		/*
		 * VECTOR MAP
		 */
		 
		 data_array = {
		 	"US": 4977,
		 	"AU": 4873,
		 	"IN": 3671,
		 	"BR": 2476,
		 	"TR": 1476,
		 	"CN": 146,
		 	"CA": 134,
		 	"BD": 100
		 };
		 
		// Load Map dependency 1 then call for dependency 2 and then run renderVectorMap function
		// loadScript("js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js", function(){
		// loadScript("js/plugin/vectormap/jquery-jvectormap-world-mill-en.js", renderVectorMap);
		//});
		

		function renderVectorMap() {
			$('#vector-map').vectorMap({
				map: 'world_mill_en',
				backgroundColor: '#fff',
				regionStyle: {
					initial: {
						fill: '#c4c4c4'
					},
					hover: {
						"fill-opacity": 1
					}
				},
				series: {
					regions: [{
						values: data_array,
						scale: ['#85a8b6', '#4d7686'],
						normalizeFunction: 'polynomial'
					}]
				},
				onRegionLabelShow: function (e, el, code) {
					if (typeof data_array[code] == 'undefined') {
						e.preventDefault();
					} else {
						var countrylbl = data_array[code];
						el.html(el.html() + ': ' + countrylbl + ' visits');
					}
				}
			});
		}
		
		/*
		 * CHAT
		 */
		 
		 var filter_input = $('#filter-chat-list'),
		 chat_users_container = $('#chat-container > .chat-list-body'),
		 chat_users = $('#chat-users'),
		 chat_list_btn = $('#chat-container > .chat-list-open-close'),
		 chat_body = $('#chat-body');
		 
		/*
		 * LIST FILTER (CHAT)
		 */
		 
		// custom css expression for a case-insensitive contains()
		jQuery.expr[':'].Contains = function (a, i, m) {
			return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
		};
		
		function listFilter(list) { 
			// header is any element, list is an unordered list
		    // create and add the filter form to the header
		    
		    filter_input.change(function () {
		    	var filter = $(this).val();
		    	if (filter) {
		            // this finds all links in a list that contain the input,
		            // and hide the ones not containing the input while showing the ones that do
		            chat_users.find("a:not(:Contains(" + filter + "))").parent().slideUp();
		            chat_users.find("a:Contains(" + filter + ")").parent().slideDown();
		        } else {
		        	chat_users.find("li").slideDown();
		        }
		        return false;
		    }).keyup(function () {
		        // fire the above change event after every letter
		        $(this).change();
		        
		    });
		    
		}
		
		// on dom ready
		listFilter(chat_users);
		
		// open chat list
		chat_list_btn.click(function () {
			$(this).parent('#chat-container').toggleClass('open');
		})
		
		chat_body.animate({
			scrollTop: chat_body[0].scrollHeight
		}, 500);
		
	};
	
	// end pagefunction

	// destroy generated instances 
	// pagedestroy is called automatically before loading a new page
	// only usable in AJAX version!

	var pagedestroy = function(){
		
		// destroy calendar
		calendar.fullCalendar('destroy');
		calendar = null;

		//destroy flots
		flot_updating_chart.shutdown();  
		flot_updating_chart=null;
		flot_statsChart.shutdown(); 
		flot_statsChart = null;

		flot_multigraph.shutdown(); 
		flot_multigraph = null;

		// destroy vector map objects
		$('#vector-map').find('*').addBack().off().remove();

		// destroy todo
		$("#sortable1, #sortable2").sortable("destroy");
		$('.todo .checkbox > input[type="checkbox"]').off();

		// destroy misc events
		$("#rev-toggles").find(':checkbox').off();
		$('#chat-container').find('*').addBack().off().remove();

		// debug msg
		if (debugState){
			root.console.log("✔ Calendar, Flot Charts, Vector map, misc events destroyed");
		} 

	}

	// end destroy
	
	// run pagefunction on load
	pagefunction();
	
	
</script>
